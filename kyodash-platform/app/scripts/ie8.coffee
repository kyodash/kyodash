
# Remove ng-cloak class so the browser incompatibility div is displayed
body = document.getElementsByTagName('body')[0]
body.removeAttribute('class')

# Remove hidden class on #browserError div
browserError = document.getElementById('browserError')
browserError.removeAttribute('class')
