#
# GenericErrorModalController controller -- for modal dialog
#

kyodashApp.controller 'GenericErrorModalController', ($scope, $uibModalInstance, $state) ->

    $scope.goHome = ->
        $uibModalInstance.dismiss()
        $state.go('home')

    $scope.reload = ->
        $uibModalInstance.dismiss()
        $state.reload()


    $scope.goEditor = ->
        $uibModalInstance.dismiss()
        $state.go('edit.details', { dashboardName: $scope.originalDashboardName})
    
