#
# Login controller -- for login modal dialog
#
kyodashApp.controller 'LoginController', ($scope, $uibModalInstance, $localForage, configService, focusService, userService) ->

    $scope.credentials = {}
    $scope.loginError = false
    $scope.loggingIn = false

    $scope.loginMessage ?= configService.authentication.loginMessage

    # Load cached username
    if userService.cachedUsername?
        $scope.credentials.username = userService.cachedUsername
        focusService.focus 'focusPassword', $scope
    else 
        focusService.focus 'focusUsername', $scope

    $scope.canLogin = ->
        return !_.isEmpty($scope.credentials.username) && !_.isEmpty($scope.credentials.password) && !$scope.loggingIn

    $scope.login = ->
        return if $scope.loggingIn
        $scope.loginError = false
        $scope.loggingIn = true
        loginPromise = userService.login $scope.credentials.username, $scope.credentials.password

        loginPromise.then (session) ->
            $scope.credentials.password = ''
            $uibModalInstance.close(session)

        loginPromise.catch (error) ->
            $scope.loginError = true
            $scope.loggingIn = false
            $scope.credentials.password = ''
            focusService.focus 'focusPassword', $scope

    $scope.cancel = ->
        $uibModalInstance.dismiss('cancel')
