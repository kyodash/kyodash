
kyodashServices.factory 'configService', (commonConfigService) ->

    # List of neighboring KyoDash instances
    # Used for:
    #   1) Push Dashboard to Environment (e.g. push from Dev to Prod)
    #   2) Data Source Proxying
    #
    # "name": Display Name.
    # "serviceUrl": Destination service endpoint.
    # "requiresAuth": Must match the configuration on the destination service.
    # "canPush": If true, enables pushing Dashboard from this instance to the destination instance.
    #
    kyodashEnvironments = [
        {
            name: 'Dev'
            serviceUrl: 'http://kyo-labs.com/api'
            requiresAuth: true
            canPush: true
        }
        {
            name: 'Localhost'
            serviceUrl: 'http://localhost:8077'
            requiresAuth: false
            canPush: false
        }
    ]

    # Convert kyodashEnvironments into the correct format for property options
    proxyOptions = _.reduce kyodashEnvironments, (options, environment) ->
        options[environment.name] = { value: environment.serviceUrl }
        return options
    , {}

    exports = 
        # Kyodash-svc endpoint
        restServiceUrl: 'http://localhost:8077'

        authentication:
            # Enable or disable authentication
            # Should match the kyodash-svc configuration
            enable: false

            # Message displayed when logging in.  Set to null/blank to disable
            loginMessage: 'Please login using your LDAP username and password.'

            # If true, the user's password will be encrypted and cached in the browser
            # This allows data sources to authenticate with the current user's credentials
            cacheEncryptedPassword: false

        # Analytics settings
        analytics:
            # Enable or disable analytic tracking for dashboards
            enable: false

        # Logging settings
        logging:
            enableDebug: false

        # New Users
        newUser:
            # Enables/disables welcome message for new users
            enableMessage: true

        
        # List of neighboring KyoDash instances
        kyodashEnvironments: kyodashEnvironments

        # Changelog location, displayed on the home page footer
        #changelogLink: 'https://.../CHANGELOG.md'

        # Overrides for Dashboard properties
        #
        # 1) Provide environment-specific default values, e.g. default URLs for Data Sources
        # 2) Set proxy options for Data Sources
        #
        dashboard:
            properties:
                dataSources:
                    options:
                        kyodashData:
                            properties:
                                url:
                                    options: proxyOptions
                        graphite:
                            properties:
                                url:
                                    # The default Graphite hostname, so it does not need to be specified in each Dashboard
                                    # (Remove if there is no appropriate default)
                                    default: 'http://sampleGraphiteHost:80'
                                proxy:
                                    options: proxyOptions
                        json:
                            properties:
                                proxy:
                                    options: proxyOptions
                        prometheus:
                            properties:
                                proxy:
                                    options: proxyOptions
                        splunk:
                            properties:
                                host:
                                    # The default Splunk hostname, so it does not need to be specified in each Dashboard
                                    # (Remove if there is no appropriate default)
                                    default: 'splunk'
                                proxy:
                                    options: proxyOptions

        # Add additional logos to the Dashboard Sidebar 
        dashboardSidebar:
            footer:
                logos: [{
                    title: 'KyoDash'
                    src: '/img/favicon32.png'
                    href: '/'
                }]

    # Merge overrides with commonConfigService
    # Settings in this file will override/extend those in the commonConfigService
    merged = _.merge(commonConfigService, exports, _["default"])

    # Add a custom welcome message to the Help page
    # Additional messages can be added, e.g. support mailing list
    merged.help[0].messages = [{
        type: 'info',
        html: 'Welcome to KyoDash!',
        icon: 'fa-info-circle'
    }];

    return merged;
