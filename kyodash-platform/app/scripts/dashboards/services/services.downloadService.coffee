
kyodashServices.factory 'downloadService', ($http, $q, $localForage, $window, analyticsService, configService, logService) ->

    exports = {

        download: (name, format, data) ->
            # Post data to /export/data endpoint, get back a URL to the file
            # Then download the file
            $http.post(configService.restServiceUrl + '/export/data', { name, format, data })
            .then (result) ->
                $window.location = result.data.url
                alertify.log('Downloaded Widget Data', 2500)
            .catch (error) ->
                alertify.error 'Error downloading Widget data', 2500

    }

    return exports
