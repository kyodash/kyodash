#
# KyodashData Data Source
#
# Retrieves data from a KyodashData bucket.  Does not use a proxy.
#
kyodashDataSources.factory 'kyodashdataDataSource', ($q, $http, configService, dataSourceFactory) ->

    runner = (options) ->

        q = $q.defer()

        # Runner Failure
        errorCallback = (error, status) ->
            if error == '' && status == 0
                # CORS error
                error = 'Cross-Origin Resource Sharing error with the server.'

            q.reject error

        # Successful Result
        successCallback = (result) ->
            q.resolve
                '0':
                    data: result.data
                    columns: null

        # Do the request, wiring up success/failure handlers
        key = _.jsExec options.key
        url = (_.jsExec(options.url) || configService.restServiceUrl) + '/data/' + encodeURIComponent(key) + '/data'

        req = $http.get url
        
        # Add callback handlers to promise
        req.then successCallback
        req.error errorCallback

        return q.promise

    dataSourceFactory.create 'KyoDashData', runner
