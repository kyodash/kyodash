
kyodashDirectives.directive 'dashboardWidget', (layoutService) ->
    {
        restrict: 'AC'
        
        link: (scope, element, attrs) ->
            $element = $(element)
            $parent = $element.parent()

            scope.$watch 'widget', (widget) ->

                # Wire-up fullscreen button if available
                if widget.allowFullscreen
                    $parent.find('.widget-fullscreen').on 'click', ->
                        $element.fullScreen(true)
                return

            scope.$watch 'layout', (layout) ->
                return unless layout?

                # Set the border width if overloaded (otherwise keep theme default)
                if layout.borderWidth?
                    $element.css('border-width', layout.borderWidth + 'px')

            return

    }
