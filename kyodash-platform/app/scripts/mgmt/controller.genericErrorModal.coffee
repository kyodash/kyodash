
#
# GenericErrorModalController controller -- for modal dialog
#
kyodashApp.controller 'GenericErrorModalController', ($scope, $uibModalInstance, $state) ->

    $scope.goHome = ->
        $uibModalInstance.dismiss()
        $state.go('home')
