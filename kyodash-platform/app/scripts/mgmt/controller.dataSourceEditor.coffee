#
# Gui Editor - Data Source controller.
#
kyodashApp.controller 'DataSourceEditorController', ($scope, $state, $stateParams, configService, dashboardService) ->

    # Store some configuration settings for the Editor
    $scope.dashboardProperties = configService.dashboard.properties
    $scope.allDataSources = configService.dashboard.properties.dataSources.options

    $scope.$watch 'editor.selectedItemIndex', ->
        $scope.dataSourceIndex = $scope.editor.selectedItemIndex
        $scope.dataSource = $scope.editor.selectedItem

    $scope.combinedDataSourceProperties = (dataSource) ->
        general = _.omit configService.dashboard.properties.dataSources.properties, 'type'

        if dataSource.type? and $scope.allDataSources[dataSource.type]?
            specific = $scope.allDataSources[dataSource.type].properties
            return _.defaults specific, general
        else
            return {}

    $scope.dataSourceMessage = ->
        $scope.allDataSources[$scope.dataSource.type]?.message

    return
