#
# EncryptString controller -- for modal dialog
#
kyodashApp.controller 'EncryptStringController', ($scope, $uibModalInstance, cryptoService, configService) ->

    $scope.fields = {}

    $scope.encrypt = ->
        return if _.isEmpty $scope.fields.value
        cryptoService.encrypt($scope.fields.value).then (result) ->
            $scope.fields.encryptedValue = result

    $scope.cancel = ->
        $uibModalInstance.dismiss('cancel')
