#
# Dashboard History controller
#
kyodashApp.controller 'DashboardHistoryController', ($scope, $stateParams, $uibModal, dashboardService) ->

    $scope.dashboardName = $stateParams.dashboardName
    $scope.itemsPerPage = 25
    $scope.currentPage = 1


    dashboardService.getDashboard($scope.dashboardName).then (dashboard) ->
        $scope.dashboard = dashboard
    dashboardService.getRevisions($scope.dashboardName).then (revisions) ->
        $scope.revisions = revisions
        $scope.revisionsCount = revisions.length
    
    $scope.diffWithLatest = (rev) ->
        # Diff dialog
        modalInstance = $uibModal.open {
            templateUrl: '/partials/revisionDiff.html'
            size: 'lg'
            scope: $scope
            controller: ['$scope', '$sce', 'rev', ($scope, $sce, rev) ->
                $scope.rev1 = rev - 1
                $scope.rev2 = rev

                $scope.previous = ->
                    $scope.rev2 = $scope.rev1
                    $scope.rev1 = $scope.rev1 - 1
                $scope.next = ->
                    $scope.rev1 = $scope.rev2
                    $scope.rev2 = $scope.rev2 + 1

                $scope.updateDiff = ->
                    dashboardService.getRevisionDiff($scope.dashboardName, $scope.rev1, $scope.rev2).then (diff) ->
                        $scope.diff = $sce.trustAsHtml(diff)

                $scope.updateDiff()

                $scope.$watch 'rev1 + rev2', ->
                    $scope.updateDiff()
            ]
            resolve: {
                rev: -> rev
            }
        }
