
#
# Listens for focus events and sets focuses on a particular element
# For use with the focusService
#

kyodashDirectives.directive 'focusOn', ->
    {
        restrict: 'AC',
        link: (scope, element, attrs) ->
            scope.$on 'focusOn', (event, name) -> 
                element[0].focus() if attrs.focusOn == name
    }
