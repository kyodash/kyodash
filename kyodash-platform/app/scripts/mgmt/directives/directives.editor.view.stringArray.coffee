
kyodashDirectives.directive 'editorViewStringArray', ->
    {
        restrict: 'EAC'
        scope: 
            label: '@'
            model: '='
            definition: '='

        templateUrl: '/partials/editor/stringArray.html'

        controller: ($scope) ->

            $scope.addArrayValue = ->
                $scope.model = [] unless $scope.model?
                $scope.model.push ''
            
            $scope.updateArrayValue = (index, value) ->
                $scope.model[index] = value

            $scope.removeArrayValue = (index) ->
                $scope.model.splice(index, 1)
        
        link: (scope, element, attrs) ->

            scope.label ?= 'Item'
            return
            
    }
