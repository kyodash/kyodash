
kyodashDirectives.directive 'editorViewInlineArray', ->
    {
        restrict: 'EAC'
        scope:
            model: '='
            definition: '='
            factory: '&'
            headingfn: '&'

        templateUrl: '/partials/editor/inlineArray.html'
        transclude: true

        controller: ($scope) ->
           
            $scope.removeItem = (index) ->
                $scope.model.splice(index, 1)

            $scope.addNewObject = ->
                $scope.model = [] unless $scope.model?
                if $scope.definition.sample?
                    $scope.model.push _.cloneDeep($scope.definition.sample)
                else 
                    $scope.model.push {}

    }
