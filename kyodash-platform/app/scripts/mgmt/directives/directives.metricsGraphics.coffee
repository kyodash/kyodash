
kyodashDirectives.directive 'metricsGraphics', ->
    {
        restrict: 'EAC'
        scope: 
            data: '='
            options: '='

        link: (scope, element) ->

            $element = $(element)
            chartWidth = $element.width()

            # Generate random element ID
            if _.isEmpty $element.prop('id')
                scope.id = 'mg-' + uuid.v4()
                $element.prop 'id', scope.id

            redraw = ->
                return unless scope.data?

                options = 
                    title: null
                    height: 200
                    width: chartWidth
                    target: '#' + scope.id
                    data: scope.data
                    interpolate: 'cardinal'
                    interpolate_tension: 0.95

                # Apply passed options
                _.assign options, scope.options

                # Wrap mouseover
                if options.mouseover?
                    getMouseoverText = options.mouseover
                    options.mouseover = (d, i) ->
                        text = getMouseoverText(d, i)
                        d3.select('#' + scope.id + ' svg .mg-active-datapoint').text(text)

                MG.data_graphic options
            
            scope.$watch 'data', (newData) ->
                redraw()

            $element.resize _.debounce ->
                chartWidth = $element.width()
                redraw()
            , 100, { leading: false, maxWait: 300 }
    }
