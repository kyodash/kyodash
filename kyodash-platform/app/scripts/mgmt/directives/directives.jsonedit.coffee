
kyodashDirectives.directive 'jsonedit', ->
    {
        restrict: 'EAC'
        scope: 
            model: '='
        replace: true 
        template: '<div ui-ace="aceOptions" ng-model="jsonValue"></div>'

        controller: ($scope, dashboardService) ->

            $scope.aceLoaded = (editor) ->
                editor.setOptions {
                    maxLines: Infinity
                    minLines: 10
                    enableBasicAutocompletion: true
                }
                editor.focus()

            # Settings for the JSON Editor
            $scope.aceOptions = 
                useWrapMode : true
                showGutter: true
                showPrintMargin: false
                mode: 'json'
                theme: 'chrome'
                onLoad: $scope.aceLoaded

            $scope.jsonValue = dashboardService.toString $scope.model

            $scope.$watch 'jsonValue', (json) ->
                # Catch parse errors due to incomplete objects
                try
                    if $scope.model?
                        _.replaceInside $scope.model, dashboardService.parse(json)
                    else
                        $scope.model = dashboardService.parse(json)
    }
