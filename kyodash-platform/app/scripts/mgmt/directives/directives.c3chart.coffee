
kyodashDirectives.directive 'c3chart', ->
    {
        restrict: 'EAC'
        scope: 
            data: '='
            options: '='

        link: (scope, element) ->

            $element = $(element)
            scope.width = $element.width()

            # Generate random element ID
            if _.isEmpty $element.prop('id')
                scope.id = 'c3-' + uuid.v4()
                $element.prop 'id', scope.id

            redraw = ->
                return unless scope.data? and scope.data.length > 0

                options = 
                    bindto: '#' + scope.id
                    data: 
                        json: scope.data
                    size:
                        height: 200
                    
                # Apply passed options
                _.merge options, scope.options

                c3.generate options
            
            scope.$watch 'data', ->
                redraw()
            
            scope.$watch 'options', ->
                redraw()

    }
