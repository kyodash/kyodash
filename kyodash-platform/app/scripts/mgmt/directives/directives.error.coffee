

kyodashDirectives.directive 'error', ->
    {
        restrict: 'CA'
        link: (scope, element, attrs) ->
            $('body').addClass('errorPage')
    }
