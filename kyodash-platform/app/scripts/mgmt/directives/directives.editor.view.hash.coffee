
kyodashDirectives.directive 'editorViewHash', ->
    {
        restrict: 'EAC'
        scope:
            label: '@'
            model: '='
        templateUrl: '/partials/editor/hash.html'

        controller: ($scope) ->

            $scope.$watch 'model', (model) ->
                $scope.hashItems = [] unless model?

            $scope.addHashValue = ->
                $scope.model = {} unless $scope.model?
                $scope.hashItems.push { key: '', value: '', _key: ''}

            $scope.updateHashKey = (hashItem) ->
                # Set value to new key
                $scope.model[hashItem.key] = $scope.model[hashItem._key] ? ''

                # Delete old key
                delete $scope.model[hashItem._key]

                # Update hidden key
                hashItem._key = hashItem.key

            $scope.updateHashValue = (hashItem) ->
                $scope.model[hashItem.key] = hashItem.value

            $scope.removeHashItem = (hashItem) ->
                delete $scope.model[hashItem.key]
                _.remove $scope.hashItems, (item) ->
                    item == hashItem

            # Initialize
            if $scope.model?
                $scope.hashItems = _.map $scope.model, (value, key) -> 
                    { key: key, value: value, _key: key}
                $scope.hashItems = _.filter $scope.hashItems, (hashItem) -> 
                    hashItem._key != '$$hashKey'
            else
                $scope.hashItems = []

        link: (scope, element, attrs) ->
            scope.label ?= 'Item'
            return

    }
