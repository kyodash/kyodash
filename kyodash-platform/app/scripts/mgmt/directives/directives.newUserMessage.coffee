
kyodashDirectives.directive 'newUserMessage', ->
    {
        restrict: 'E'
        scope: { }
        templateUrl: 'partials/newUserMessage.html'

        link: (scope, element, attrs) ->
            return

        controller: ($scope, $timeout, configService, logService, userService) ->

            $scope.message = configService.newUser.welcomeMessage
            $scope.iconClass = configService.newUser.iconClass

            $scope.canDisplay = ->
                configService.newUser.enableMessage and userService.isNewUser

            $scope.dismiss = ->
                userService.notNewUser()

            # Automatically remove New User quality after a fixed period of time on the site
            # This won't hide the message if it's currently displayed
            duration = configService.newUser.autoDecayDuration
            if _.isNumber duration
                t = $timeout _.partial(userService.notNewUser, false), duration * 1000
            
    }
