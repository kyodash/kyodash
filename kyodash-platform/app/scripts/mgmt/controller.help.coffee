#
# Help controller -- for viewing help pages
#
kyodashApp.controller 'HelpController', ($scope, $location, configService) ->

    $scope.config = configService

    $scope.menu = configService.help

    $scope.selectItem = (item) ->
        $scope.selectedItem = item
        $location.search 'q', item.name

    $scope.feelingLucky = ->
        $scope.$broadcast 'feelingLucky'

    $scope.findItem = (name) ->
        $scope.$broadcast 'findItem', { name: name }

    # Initialization
    q = $location.search().q
    if q? then $scope.q = q
