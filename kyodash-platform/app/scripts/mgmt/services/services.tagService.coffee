

kyodashServices.factory 'tagService', ($http, configService) ->

    {
        # List of all tags
        getTags: (callback) ->
            q = $http.get(configService.restServiceUrl + '/tags')
            q.success (tags) -> callback(tags) if _.isFunction(callback)
            q.error -> alertify.error 'Cannot connect to kyodash-svc (getTags)', 2500


        # List of autocomplete hints suggested when searching
        getSearchHints: (callback) ->
            q = $http.get(configService.restServiceUrl + '/searchhints')
            q.success (searchhints) -> callback(searchhints) if _.isFunction(callback)
            q.error -> alertify.error 'Cannot connect to kyodash-svc (getSearchHints)', 2500
    }
