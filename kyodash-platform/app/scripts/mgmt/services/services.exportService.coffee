

kyodashServices.factory 'exportService', ($http, configService) ->

    {
        # List of all tags
        exportAsync: (dashboardName, format, params, callback) ->
            uri = configService.restServiceUrl + '/export/' + dashboardName + '/' + format
            if params? && _.keys(params).length > 0
                paramStrings = _.map _.pairs(params), (pair) ->
                    pair[0] + '=' + pair[1]
                uri += '?' + paramStrings.join('&')

            $http.post(uri).success (result) ->
                if _.isFunction(callback) then callback(result)

        getStatus: (statusUrl, callback) ->
            $http.get(statusUrl).success (result) ->
                if _.isFunction(callback) then callback(result)
    }
