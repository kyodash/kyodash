ge governing permissions and limitations under the License.
 */ 
 
/* CORS middleware */
exports.allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', req.headers.origin || '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Cache-Control', 'no-store');
    res.header('Pragma', 'no-cache');
    res.header('Expires', '0');

    /* intercept OPTIONS method */
    if ('OPTIONS' == req.method) {
        res.status(200);
        res.send();
    }
    else {
        next();
    }
};
