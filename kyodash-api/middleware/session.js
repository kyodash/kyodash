/* Session middleware
 * 
 * If `session` is provided in the query string, the session key will be used to
 * load a Session object from the database.  The Session will be attached to the 
 * request.
 * 
 * If an invalid session key is provided, error 401 will be returned.
 */
var auth = require('../../kyodash/kyodash-svc/routes/auth.js');

exports.sessionLoader = function(req, res, next) {
    /* If ?session= provided, attempt to load it into req.session and req.user */
    if (req.query.session != undefined) {
        auth.validateSession(req.query.session)
        .then(function (session) {
            req.session = session;
            req.user = session.user;
            next();
        })
        .catch(function () {
            res.status(401).send('Authentication failed: session key provided but not valid.');
        })
    } else {
        next();
    }
};
