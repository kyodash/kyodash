
 
/* 
 * API for Tags
 */

var _ = require('lodash'),
    mongoose = require('mongoose'),
    api = require('./api');

var Dashboards = mongoose.model('dashboard2');

/* Get all tags */
exports.get = function (req, res) {
    Dashboards.find({ deleted: false })
        .select('tags')
        .exec(function (err, dashboards) {
            if (err) {
                console.log(err);
                res.status(500).send(err);
            } else {
                var tags = _(dashboards)
                    .map('tags')
                    .flatten()
                    .compact()
                    .sortBy()
                    .uniq(true)
                    .value();
                res.send(tags);
            }
        });
};

/* Get all tags and name parts for autocomplete */
exports.getSearchHints = function (req, res) {
    Dashboards.find({ deleted: false })
        .select('name tags')
        .exec(function (err, dashboards) {
            if (err) {
                console.log(err);
                res.status(500).send(err);
            } else {
                var tags = _(dashboards)
                    .map('tags')
                    .flatten()
                    .compact()
                    .sortBy()
                    .value();

                var names = _(dashboards)
                    .map('name')
                    .sortBy()
                    .value();

                res.send(_.uniq(_.union(tags, names), true));
            }
        });
};
